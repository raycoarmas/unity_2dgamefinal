﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{

    public Transform playerTransform;
    public Transform borderA;
    public Transform borderB;
    public float speed;
    public bool facingRight;
    private bool death = false;
    private AudioSource source;
    private int scoreKill = 110;
    // Use this for initialization
    void Start()
    {
        source = GetComponent<AudioSource>();
        StartCoroutine(GameLoop());
        if (facingRight)
        {
            Vector3 scale = transform.localScale;
            scale.x *= -1;
            transform.localScale = scale;
        }
    }
    void Flip()
    {
        facingRight = !facingRight;
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }

    private IEnumerator GameLoop()
    {
        while (!death)
        {
            yield return StartCoroutine(MoveObject(transform, borderB.position, borderA.position, 10.0f));
            Flip();
            yield return StartCoroutine(MoveObject(transform, borderA.position, borderB.position, 10.0f));
            Flip();
        }
    }


    private IEnumerator MoveObject(Transform transform, Vector3 startPos, Vector3 endPos, float time)
    {
        var i = 0.0f;
        var rate = 1.0f / time;
        while (i < 1.0f)
        {
            i += Time.deltaTime * rate * speed;
            transform.position = Vector3.Lerp(startPos, endPos, i);
            yield return null;
        }
    }

    public void Death()
    {
        if (!death)
        {
            transform.Find("damageArea").gameObject.SetActive(false);
            speed = 0;
            death = true;
            ScoreController.score += scoreKill;
            source.Play();
            StartCoroutine(sounds());
        }
    }

    private IEnumerator sounds()
    {

        while (source.isPlaying)
        {
            yield return null;
        }
        Destroy(this.gameObject);
    }

    public void hide()
    {
        GetComponent<Renderer>().enabled = false;
        GetComponent<BoxCollider2D>().enabled = false;
    }

    public bool isDeath()
    {
        return death;
    }
}
