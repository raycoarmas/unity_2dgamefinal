﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PayasoController : MonoBehaviour {

	AudioSource source;
	private bool played = false;
	void Awake()
    {
		source = GetComponent<AudioSource>();
        DontDestroyOnLoad(this);
    }
	
	// Update is called once per frame
	void Update () {
		if(!source.isPlaying && played){
			Destroy(this.gameObject);
		}
	}

	public void Play(){
       	source.Play();
		played = true;
	}
}
