﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreController : MonoBehaviour
{
    public static int score; 
    Text text; 


    void Awake()
    {
        text = GetComponent<Text>();
        score = 0;
    }


    void Update()
    {
        if(score < 0){
            score = 0;
        }
        text.text = "Score: " + score;
    }
}
