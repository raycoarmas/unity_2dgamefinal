﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinnerController : MonoBehaviour {

	private AudioSource audioSource;

	private bool winGame;
	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource>();
		winGame= false;
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.CompareTag("Player"))
        {
			audioSource.Play();
			winGame= true;
        }
	}

	public bool isWinner(){
		return winGame;
	}

}
