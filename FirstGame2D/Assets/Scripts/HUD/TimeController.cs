﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimeController : MonoBehaviour
{

    Text text;
	private float timer;
    void Start()
    {
        text = GetComponent<Text>();
    }

    void Update()
    {
		timer += Time.deltaTime;
        float minutes = Mathf.Floor(timer / 60);
        float seconds = Mathf.RoundToInt(timer % 60);

        text.text = "Timer: " + minutes.ToString("00") + ":"+ seconds.ToString("00");
    }
}
