﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GMController : MonoBehaviour
{
    public PauseManager pauseManager;
    public Text messages;
    public PlayerController playerController;

    public WinnerController winnerController;

    public GameObject endPanel;
    
    // Use this for initialization
    void Start()
    {
        //en el evento del boton reset y MainMenu esta linea no funciona, se setea a 1 pero al iniciarse el juego se vuelve a quedar a 0
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerController.isDeath())
        {
            messages.text = "You are death";
            endPanel.SetActive(true);
            Time.timeScale = 0;
        }
        if (winnerController.isWinner())
        {
            messages.text = "You won the game";
            endPanel.SetActive(true);
            Time.timeScale = 0;
        }
    }

    public void RestGame(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void MainMenu(){
        SceneManager.LoadScene("MainMenu");
    }

}
