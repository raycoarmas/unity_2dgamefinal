﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlataform : MonoBehaviour
{
    public Transform pointA;
    public Transform pointB;
    // Use this for initialization
    void Start()
    {
        StartCoroutine(GameLoop());
    }


    private IEnumerator GameLoop()
    {
        while (true)
        {
           yield return StartCoroutine(MoveObject(transform, pointA.position, pointB.position, 3.0f));
           yield return StartCoroutine(MoveObject(transform, pointB.position, pointA.position, 3.0f));
        }
    }


    private IEnumerator MoveObject(Transform transform, Vector3 startPos, Vector3 endPos, float time)
    {
        var i= 0.0f;
        var rate= 1.0f/time;
        while (i < 1.0f)
        {
            i += Time.deltaTime * rate;
            transform.position = Vector3.Lerp(startPos, endPos, i);
            yield return null;
        }
    }


   void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.transform.SetParent(transform);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        other.gameObject.transform.SetParent(null);
    }

}
