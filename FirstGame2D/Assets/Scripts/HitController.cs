﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitController : MonoBehaviour
{

    public PlayerController playerController;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Enemy") && !this.gameObject.CompareTag("Enemy"))
        {
            if(!other.gameObject.GetComponent<EnemyMove>().isDeath()){
                other.gameObject.GetComponent<Animator>().SetTrigger("Death");
            }
        }
        if (other.gameObject.CompareTag("Player") && this.gameObject.CompareTag("Enemy"))
        {
            playerController.EnemyHit();           
        }
        if(this.gameObject.CompareTag("Bullet") && !other.gameObject.CompareTag("Bullet")){
            GetComponent<Animator>().SetTrigger("Hit");
            GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
        }
    }
    public void Destroy(){
        Destroy(this.gameObject);
    }
}
