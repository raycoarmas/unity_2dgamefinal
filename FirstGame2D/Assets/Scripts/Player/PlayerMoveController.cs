﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveController : MonoBehaviour {

    private PlayerController playerController;
    private Rigidbody2D rb2d;
    void Start()
    {
        playerController = GetComponent<PlayerController>();
        rb2d = GetComponent<Rigidbody2D>();
    }
	
	void Update () {
		 playerController.grounded = Physics2D.Linecast(transform.position, playerController.groundCheck.position, LayerMask.GetMask("Ground"));

        if (playerController.grounded)
        {
            playerController.jumping = false;
            playerController.animator.ResetTrigger("jump");
        }

        if (Input.GetButtonDown("Jump") && playerController.grounded)
        {
            playerController.jump = true;
            playerController.jumping = true;
        }
	}

	void FixedUpdate()
	{
        if (!playerController.isHitted())
        {
		    float x = Input.GetAxis("Horizontal");
            playerController.animator.SetFloat("move", Mathf.Abs(x));
            move(x);
            if (x > 0 && !playerController.facingRight)
            {
                Flip();
            }
            else if (x < 0 && playerController.facingRight)
            {
                Flip();
            }
        }
        else
        {
            move((playerController.facingRight ? -1.5f : 1.5f));
        }

        
        if (playerController.jump)
        {
            playerController.animator.SetTrigger("jump");
            rb2d.AddForce(new Vector2(0f, playerController.jumpForce), ForceMode2D.Impulse);
            playerController.jump = false;
        }
	}
    void Flip()
    {
        playerController.facingRight = !playerController.facingRight;
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }

    void move(float move)
    {
        Vector2 movement = transform.right * move * playerController.speed * Time.deltaTime;
        rb2d.MovePosition(rb2d.position + movement);

    }
	
}
