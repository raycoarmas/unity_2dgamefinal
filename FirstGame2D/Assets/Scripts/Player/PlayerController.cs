﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PlayerController : MonoBehaviour
{

    [HideInInspector] public bool jump = false;
    [HideInInspector] public Animator animator;
    [HideInInspector] public bool facingRight = true;
    [HideInInspector] public bool jumping = false;
    [HideInInspector] public bool grounded = false;
    public float speed;
    public float jumpForce;
    public Transform groundCheck;
    private bool hitted = false;
    private int liveCount = 2;
    private string live = "live";
    public float hitDelay;
    private float lastHitted;
    void Start()
    {
        animator = GetComponent<Animator>();
        lastHitted = hitDelay;
    }

    void FixedUpdate()
    {
        lastHitted += Time.deltaTime;
    }

    public void EnemyHit()
    {        
        if(!hitted && hitDelay <= lastHitted && !isDeath()){
            animator.SetTrigger("Hit");
            lastHitted = 0;
            GetComponent<AudioSource>().Play();
        }
    }

    public void setHittedTrue(){
        hitted = true;
    }
    public void EnemyStopHit()
    {
        GameObject actualLive = GameObject.Find(live+liveCount.ToString());
        actualLive.SetActive(false);
        liveCount -= 1;
        hitted = false;
    }

    public bool isDeath(){
        return liveCount <= 0;
    }

    public bool isHitted(){
        return hitted;
    }
}
