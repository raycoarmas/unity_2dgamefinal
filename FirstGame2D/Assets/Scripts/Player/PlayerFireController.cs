﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFireController : MonoBehaviour
{
    public float shotDelay;
    private float lastShot = 0;
    public GameObject gun;
    public GameObject bullet;
    private int shotLostPoints = 10;
    private PlayerController playerController;
    void Start()
    {
        playerController = GetComponent<PlayerController>();
    }
    void Update() {
        lastShot += Time.deltaTime;
    }

    void FixedUpdate()
    {
        fire();
    }
   
    void Shot()
    {
        if (shotDelay <= lastShot)
        {
            lastShot = 0;
            ScoreController.score -= shotLostPoints;
            GameObject bulletShoted = Instantiate(bullet, gun.transform.position, Quaternion.identity);
            bulletShoted.GetComponent<Rigidbody2D>().AddForce(new Vector2(300f, 0f) * (!playerController.facingRight ? -1 : 1));
        }
    }

    void fire()
    {
        if (Input.GetAxisRaw("Fire1") > 0 && (!playerController.jumping || !playerController.jump))
        {
            playerController.animator.SetBool("shot", true);
            Shot();
        }
        else
        {
            playerController.animator.SetBool("shot", false);
        }
    }

}
